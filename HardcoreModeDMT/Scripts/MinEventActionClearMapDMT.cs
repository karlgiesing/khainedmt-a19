using System.Collections.Generic;
using System.Reflection;
using UnityEngine;

/// <summary>
/// Action to clear the player's map, which can be triggered by an event.
/// Example XML to clear the map when the player is respawned:
/// <code>
/// &lt;triggered_effect trigger="onSelfRespawn" action="ClearMap, Mods" />
/// </code>
/// </summary>
public class MinEventActionClearMap : MinEventActionTargetedBase
{
    private const BindingFlags _NonPublicFlags = BindingFlags.NonPublic | BindingFlags.Instance;
    
    /// <inheritdoc />
    public override void Execute(MinEventParams _params)
    {
        // This is now a public static method, in case something else wants to use it
        // (e.g. a "book" that clears the map, or something)
        ClearMap(this.targets[0] as EntityPlayer);
    }

    /// <summary>
    /// Clears the map for the given player.
    /// </summary>
    /// <param name="player"></param>
    public static void ClearMap(EntityPlayer player)
    {
        if (player != null
            && player.ChunkObserver != null
            && player.ChunkObserver.mapDatabase != null)
            ClearMapChunkDatabase(player.ChunkObserver.mapDatabase);
    }

    private static void ClearMapChunkDatabase(MapChunkDatabase mapDatabase)
    {
        Log.Out("Clearing player map chunk database...");

        // The map chunk data is in private instance fields in the base type
        var type = typeof(MapChunkDatabase).BaseType;

        var catalog = (DictionaryKeyList<int, int>)type.GetField("catalog", _NonPublicFlags).GetValue(mapDatabase);
        if (catalog != null)
            catalog.Clear();

        var database = (DictionarySave<int, ushort[]>)type.GetField("database", _NonPublicFlags).GetValue(mapDatabase);
        if (database != null)
            database.Clear();

        var dirty = (Dictionary<int, bool>)type.GetField("dirty", _NonPublicFlags).GetValue(mapDatabase);
        if (dirty != null)
            dirty.Clear();

        Log.Out("Player map chunk database cleared.");
    }
}